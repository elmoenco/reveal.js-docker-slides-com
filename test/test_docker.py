import requests

def test_revealjs():
	revealjs_resp = requests.get('http://localhost:8000/index.html')
	#revealjs_resp = requests.get('https://google.nl')
	assert revealjs_resp.status_code == 200
	assert b'<meta http-equiv="refresh" content="0; url=./slides-com/index.html" />' in revealjs_resp.content

test_revealjs()
