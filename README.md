# Dockerimage for running reveal.js with plugins
Forked from https://github.com/agiled-de/reveal.js-docker

This configuration easily allows to present slides.com slide-deck.

This images makes it easy to use
[reveal.js](https://github.com/hakimel/reveal.js/) with docker. It supports the
use of the following plugins:

* [MathJax](https://github.com/hakimel/reveal.js/#mathjax) Enables you to write
  LaTeX formulas in your presentations
* [reveal.js-menu](https://github.com/denehyg/reveal.js-menu) Adds a menu to
  test themes, transitions and jump to slides
* [Chalkbord](https://github.com/rajgoel/reveal.js-plugins/tree/master/chalkboard)
  Write on your slides or on a chalkboard
* [Reveal.js-Title-Footer](https://github.com/e-gor/Reveal.js-Title-Footer)
  Adds a footer that will stay on every slide
* [Charts](https://gitlab.com/dvenkatsagar/reveal-chart/) To create charts
  within your presentation
* [vis.js](https://github.com/almende/vis) To create graphs in your
  presentation. This is not a plugin, it just installs vis.js and makes it
  available

## Run the Docker image
The image can be started with:

```
docker run -p 8000:8000 \
           -p 35729:35729 \
           -v ./slides-com:/revealjs/slides-com \
           reveal.js-docker-slides-com:latest
```

## Access reveal.js
reveal.js will be available on ```localhost:8000```.

## Present slides.com slide-deck
slides.com is basically reveal.js. You can export a slide deck as a zip package for offline usage. (provided you have the slides.com lit/pro subscription)
Copy the content of the main directory in the package into the slides-com directory and build the container.
